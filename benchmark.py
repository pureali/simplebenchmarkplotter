import numpy as np
import matplotlib.pyplot as plt
import time
from enum import Enum
import os, sys
# cwd=os.getcwd()
# sys.path.insert(0,cwd)
from benchmarkpkg.recordwriter import RecordWriter
BenchmarkTypes=Enum("BenchmarkTypes","Time Io Max Blender Pc")
class Benchmark:
    
    def autolabel(self,rects,ax):
        """Attach a text label above each bar in *rects*, displaying its height."""
        for rect in rects:
            height = rect.get_height()
            if (height==0):
                continue
            ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')


    def initialize(self):
        self.currentStage=0
        
        self.time=0
        self.timeTotal=0
      
      
        self.ioTime=0
        self.ioTimeTotal=0
              
        self.maxTime=0
        self.maxTimeTotal=0
        
        self.blenderTime=0
        self.blenderTimeTotal=0
        
        self.pcTime=0
        self.pcTimeTotal=0
        
        
    def __init__(self):
        
        self.initialize()
        self.StageResult={"stage":0,"time":0,"io":0,"max":0,"blender":0,"pc":0}
        self.results=[]
        self.excelWriter=RecordWriter()
    def mergeResults(self):
        newResults=[]
        stagesDone=[]
        for x in range(0,len(self.results)):
            xResult=self.results[x]
            stage=xResult['stage']
            if (stage in stagesDone):
                continue
            for y in range(x+1,len(self.results)):
                yResult=self.results[y]
                stage2=yResult['stage']
                if (stage==stage2):
                    xResult['time']=xResult['time']+yResult['time']
                    xResult['io']=xResult['io']+yResult['io']
                    xResult['max']=xResult['max']+yResult['max']
                    xResult['blender']=xResult['blender']+yResult['blender']
                    xResult['pc']=xResult['pc']+yResult['pc']
            stagesDone.append(stage)
            newResults.append(xResult)
        
        sequencedResults=[]
        previousStage=1
        for x in newResults:
            stage=x['stage']
        
            if (stage-previousStage<=1):
                sequencedResults.append(x)
                
            else:
                for y in range(previousStage+1,stage):
                    missingResult={"stage":y,"time":0,"io":0,"max":0,"blender":0,"pc":0}
                    sequencedResults.append(missingResult)
                sequencedResults.append(x)
            previousStage=stage    
        
        #just in case last stage is 18, then we still need to append one stage
        for z in range(previousStage+1,20):
            missingResult={"stage":z,"time":0,"io":0,"max":0,"blender":0,"pc":0}
            sequencedResults.append(missingResult)
            
        return sequencedResults
                    
                
            
            
        # for x in range(1,20):
        #     if (x in stagesDone):
        #         continue
        #     missingResult={"stage":x,"time":0,"io":0,"max":0,"blender":0,"pc":0}
        #     newResults.append(missingResult)
            
            
        return newResults
                    
                    
                    
                
            
            
    def plotResults(self,fileName,stages,stageTimes,ioTime,maxTime,blenderTime,pcTime,totalTime):
        x=np.arange(len(stages))
        width=0.15
        fig,ax=plt.subplots()
        #extend totaltime to same length as other stages
        totalTimeExtended=np.zeros(len(stages))
        totalTimeExtended[len(stages)-1]=totalTime
        
        r1=ax.bar(x+0.00,stageTimes,width,label="Stage-Time")
        r2=ax.bar(x+0.15,ioTime,width,label="IO-Time")
        r3=ax.bar(x+0.30,maxTime,width,label="Max-Time")
        r4=ax.bar(x+0.45,blenderTime,width,label="Blender-Time")
        r5=ax.bar(x+0.60,pcTime,width,label="PC-Time")
        r6=ax.bar(x+0.75,totalTimeExtended,width,label="Total-time",)
        
        ax.set_ylabel("Time(s)")
        ax.set_xlabel("Stages")
        ax.set_title("File:"+str(fileName)+": Time taken by stages")
        ax.set_xticks(x)
        ax.set_xticklabels(stages)
        ax.legend()
        
        self.autolabel(r1,ax)
        self.autolabel(r2,ax)
        self.autolabel(r3,ax)
        self.autolabel(r4,ax)
        self.autolabel(r5,ax)
        self.autolabel(r6,ax)
        

        #fig.tight_layout()

        plt.show()

        
    def getSumList(self,slist):
        totalTime=0
        for x in slist:
            totalTime=totalTime+x
        return totalTime
            
    def getResults(self,fileName, stagesDescription,totalTime):
        stages=[]
        stageTimes=[]
        ioTime=[]
        maxTime=[]
        blenderTime=[]
        pcTime=[]
        results=self.mergeResults()
        for x in results:
            #print(x["stage"])
            stages.append(x["stage"])
            stageTimes.append(x["time"])
            ioTime.append(x["io"])
            maxTime.append(x["max"])
            blenderTime.append(x["blender"])
            pcTime.append(x["pc"])
        summedIoTime=self.getSumList(ioTime)
        summedMaxTime=self.getSumList(maxTime)
        summedblenderTime=self.getSumList(blenderTime)
        summedPcTime=self.getSumList(pcTime)
        
        
        self.excelWriter.writeTime(fileName,stages,stageTimes,stagesDescription,summedIoTime,summedMaxTime,summedblenderTime,summedPcTime,totalTime)
        self.plotResults(fileName,stages,stageTimes,ioTime,maxTime,blenderTime,pcTime,totalTime)
        
        
        
            
            
            
    def getStage(self):
        return self.currentStage
    def setStage(self,stage):
        self.currentStage=stage
        #self.time=time.time()
        self.startTime(BenchmarkTypes.Time)
    def endStage(self):
        self.endTime(BenchmarkTypes.Time)
        print("Stage {0} ended. TotalTime: {1}, IOTime: {2}, 3DSMaxTime: {3}, BlenderTime: {4}, PolygonCruncherTime: {5}".format(self.getStage(),self.timeTotal,self.ioTimeTotal,self.maxTimeTotal,self.blenderTimeTotal,self.pcTimeTotal))
        self.StageResult["stage"]=self.getStage()
        self.StageResult["time"]=int(round(self.timeTotal,0))
        self.StageResult["io"]=int(round(self.ioTimeTotal,0))
        self.StageResult["max"]=int(round(self.maxTimeTotal,0))
        self.StageResult["blender"]=int(round(self.blenderTimeTotal,0))
        self.StageResult["pc"]=int(round(self.pcTimeTotal,0))
        self.results.append(self.StageResult.copy())
        self.initialize() #to reset variables
        
    
    def startTime(self,benchmarkType):
        if (benchmarkType==BenchmarkTypes.Time):
            self.time=time.time()
        elif (benchmarkType==BenchmarkTypes.Max):
            self.maxTime=time.time()
        elif (benchmarkType==BenchmarkTypes.Io):
            self.ioTime=time.time()
        elif (benchmarkType==BenchmarkTypes.Blender):
            self.blenderTime=time.time()
        elif (benchmarkType==BenchmarkTypes.Pc):
            self.pcTime=time.time()
        
        
        
    def endTime(self,benchmarkType):
        if (benchmarkType==BenchmarkTypes.Time):
            self.timeTotal+=time.time()-self.time
        elif (benchmarkType==BenchmarkTypes.Max):
            self.maxTimeTotal+=time.time()-self.maxTime
        elif (benchmarkType==BenchmarkTypes.Io):
            self.ioTimeTotal+=time.time()-self.ioTime
        elif (benchmarkType==BenchmarkTypes.Blender):
            self.blenderTimeTotal+=time.time()-self.blenderTime
        elif (benchmarkType==BenchmarkTypes.Pc):
            self.pcTimeTotal+=time.time()-self.pcTime
        
        
        
        
        
if __name__=="__main__":
    print("Testing")
    benchmark=Benchmark()
    
    benchmark.setStage(1)
    
    benchmark.startTime(BenchmarkTypes.Io)
    for i in range(0,1):
        time.sleep(3)
    benchmark.endTime(BenchmarkTypes.Io)
    
    benchmark.startTime(BenchmarkTypes.Max)
    for i in range(0,2):
        time.sleep(2)
    benchmark.endTime(BenchmarkTypes.Max)
    
    
    benchmark.startTime(BenchmarkTypes.Blender)
    for i in range(0,2):
        time.sleep(1)
    benchmark.endTime(BenchmarkTypes.Blender)
    
    
    benchmark.startTime(BenchmarkTypes.Pc)
    for i in range(0,2):
        time.sleep(1)
    benchmark.endTime(BenchmarkTypes.Pc)
    
    
    benchmark.endStage()
    
    
    
    
    
    
    
    
    
    
    
    #stage 2
    benchmark.setStage(2)
    benchmark.startTime(BenchmarkTypes.Io)
    for i in range(0,2):
        time.sleep(1)
    benchmark.endTime(BenchmarkTypes.Io)
   
    benchmark.startTime(BenchmarkTypes.Max)
    for i in range(0,4):
        time.sleep(1)
    benchmark.endTime(BenchmarkTypes.Max)
    benchmark.startTime(BenchmarkTypes.Io)
    for i in range(0,2):
        time.sleep(1)
    benchmark.endTime(BenchmarkTypes.Io)
    
   
    
       
    benchmark.startTime(BenchmarkTypes.Pc)
    for i in range(0,2):
        time.sleep(1)
    benchmark.endTime(BenchmarkTypes.Pc)
   
    
    
    
    benchmark.startTime(BenchmarkTypes.Blender)
    for i in range(0,2):
        time.sleep(2)
    benchmark.endTime(BenchmarkTypes.Blender)
    
    benchmark.endStage()
    
    #gap test       
    benchmark.setStage(7)
    
    benchmark.startTime(BenchmarkTypes.Io)
    for i in range(0,1):
        time.sleep(3)
    benchmark.endTime(BenchmarkTypes.Io)
    benchmark.endStage()
    
    # #stage 3 executed two times
    # benchmark.setStage(3)
    # benchmark.startTime(BenchmarkTypes.Io)
    # for i in range(0,2):
    #     time.sleep(1)
    # benchmark.endTime(BenchmarkTypes.Io)
   
    # benchmark.startTime(BenchmarkTypes.Max)
    # for i in range(0,2):
    #     time.sleep(1)
    # benchmark.endTime(BenchmarkTypes.Max)
    # benchmark.startTime(BenchmarkTypes.Io)
    # for i in range(0,2):
    #     time.sleep(1)
    # benchmark.endTime(BenchmarkTypes.Io)
    # benchmark.endStage()
    
    # #repeat stage 3
    # benchmark.setStage(3)
    # benchmark.startTime(BenchmarkTypes.Io)
    # for i in range(0,2):
    #     time.sleep(1)
    # benchmark.endTime(BenchmarkTypes.Io)
   
    # benchmark.startTime(BenchmarkTypes.Max)
    # for i in range(0,2):
    #     time.sleep(1)
    # benchmark.endTime(BenchmarkTypes.Max)
    # benchmark.startTime(BenchmarkTypes.Io)
    # for i in range(0,2):
    #     time.sleep(1)
    # benchmark.endTime(BenchmarkTypes.Io)
    # benchmark.endStage()
    
    
    
    stagesDescription=["test1","test2"]
    benchmark.getResults("test.txt",stagesDescription,50)
    
        
        