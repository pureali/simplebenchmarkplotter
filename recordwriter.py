from openpyxl.workbook.workbook import Workbook
import pandas as pd
import openpyxl
from io import StringIO
import os
manualDescription=[
"Zero Stage no description",
"Pipeline Initializing",
"Standardizing file types", 
"Creating High Poly Mesh",
"Splitting High Poly Parts",
"Extracting Hierarchy Dataline", 
"Extracting Shape Metrics",
"Producing Medium Poly Models",
"Splitting Medium Poly Models",
"Predicting Best Low Poly Settings",
"Initialising the Crunch Process",
"Extracting High Poly Mesh Data",
"Decimating medium Poly Meshes",
"Extracting Low Poly Mesh Data",
"Calculating HP/LP Similarity",
"Calculating LP models",
"Updating decimation parameters",
"Decimating Shallow Copy Objects",
"Unwrapping UVs",
"Rebuilding the models",
"Process Completed",
]
class RecordWriter:
    def __init__(self):
        print("Record writer initiated")
        self.file="./results.xlsx"
        
        #self.writer=pd.ExcelWriter(self.file,engine='openpyxl')
        
        #self.writer2=pd.ExcelWriter(self.file+"2",engine='xlsxwriter')
        
        self.append=False
        if (os.path.exists(self.file)):
            self.append=True
        
    
    def findStageIndex(self, stageNum,stages):
        for i in range(0,len(stages)):
            if (stageNum==stages[i]):
                return i
        return -1
    def writeTime(self,fileName,stages,times, stagesDescription,summedIoTime,summedMaxTime,summedblenderTime,summedPcTime,totalTime):
        print("FileName in writeTime:"+str(fileName))
        print("Stages count:"+str(len(stages))+" times count:"+str(len(times)))
        print("Stages:"+str(stages))
        print("Times:"+str(times))
        
        flist=fileName.split("/")
        flsh=""
        for x in range(0,len(flist)-1):
         flsh=flsh+"/"+flist[x]
         #root=flsh[1:]
        shortName=flist[len(flist)-1]
        fileSize=0
        try:
         fileSize=os.path.getsize(fileName)
        except Exception as ep:
            print(ep)
        fileSize=fileSize/1024 #in kb
        fileSize=fileSize/1024 #in mb's
        totalStages=len(stages)
        totalTime2=0
        for x in times:
         totalTime2=totalTime2+x
         
        print("Totaltime Original: {0} Total-time estimated: {1}".format(totalTime,totalTime2))
        
         

        if (self.append==True):
         stagesdf="Stage,Description,"+str(shortName)+"\n"
        else:    
         stagesdf="Stage, Description,"+str(shortName)+"\n"
        if (self.append==True):
         stagesdf2=str(shortName)+"\n"
        else:
         stagesdf2=stagesdf
        
        i=-1
        for x in stages:
            i+=1
            stage=x
            time=times[i]
            #description=stagesDescription[i]
            description=manualDescription[i]
            if (self.append==True):
             #stagesdf+=str(stage)+","+str(time)+"\n"
             stagesdf+=str(stage)+","+str(description)+","+str(time)+"\n"
            else:    
             stagesdf+=str(stage)+","+str(description)+","+str(time)+"\n"
            

#lets create another stagesdf for merging the stages time
        stageDone=[]
        for s in range(1,30):#loop stages because we want all the stages in the excel file even if it has not been executed
          #find the stage index in stages list
          i=self.findStageIndex(s,stages) 
          if (i>=0):
             stage=stages[i]
             #perform a simple validation
             if (stage!=s):
                 print("*******Validation error in RecordWriter*********")
             time=times[i]
             description=manualDescription[s]
             for y in range(i+1,len(stages)):
                stageNext=stages[y]
                if (stageNext==stage):
                    time+=times[y]
            
          else:#stage was not found in list of stages
              time=0
              description=""
              stage=s
              if (s>20):
                  continue
              if (s<len(manualDescription)):
               description=manualDescription[s]

          if (self.append==True):
             stagesdf2+=str(time)+"\n"
          else:    
             stagesdf2+=str(stage)+","+str(description)+","+str(time)+"\n"  



        ofileSize=fileSize
        fileSize=round(fileSize,1)
        if (fileSize==0):
            fileSize=round(ofileSize,3)         
        
        
        totalMins=totalTime/60
        totalHours=totalMins/60

        if (self.append==True):
         stagesdf+="Size (MB),"+str(fileSize)+"\n"
         stagesdf+="TotalSeconds,"+str(totalTime)+"\n"
         stagesdf+="TotalMins,"+str(totalTime/60)+"\n"
         stagesdf+="TotalHours,"+str(totalHours)+"\n"

         
         stagesdf2+=str(summedIoTime)+"\n"
         stagesdf2+=str(summedMaxTime)+"\n"
         stagesdf2+=str(summedblenderTime)+"\n"
         stagesdf2+=str(summedPcTime)+"\n"
         
         stagesdf2+=str(fileSize)+"\n"
         stagesdf2+=str(totalTime)+"\n"
         stagesdf2+=str(totalMins)+"\n"
         stagesdf2+=str(totalHours)+"\n"
        else:    
         stagesdf+=",Size (MB),"+str(fileSize)+"\n"
         stagesdf+=",TotalSeconds,"+str(totalTime)+"\n"
         stagesdf+=",TotalMins,"+str(totalTime/60)+"\n"
         stagesdf+=",TotalHours,"+str(totalHours)+"\n"
         
         stagesdf2+=",IO-Time,"+str(summedIoTime)+"\n"
         stagesdf2+=",Max-Time,"+str(summedMaxTime)+"\n"
         stagesdf2+=",Blender-Time,"+str(summedblenderTime)+"\n"
         stagesdf2+=",PC-Time,"+str(summedPcTime)+"\n"
         
         stagesdf2+=",Size (MB),"+str(fileSize)+"\n"
         stagesdf2+=",TotalSeconds,"+str(totalTime)+"\n"
         stagesdf2+=",TotalMins,"+str(totalTime/60)+"\n"
         stagesdf2+=",TotalHours,"+str(totalHours)+"\n"
       
        
        

        print("stagesdf: "+str(stagesdf))
        print("stagesdf2 after: "+str(stagesdf2))
       
        stagesdf=StringIO(stagesdf)
        stagesdf2=StringIO(stagesdf2)
        
        
        #df=pd.DataFrame({'File':fileName,'Stage':stages,'TimeTaken':times})
        
        df=pd.read_csv(stagesdf)
        df2=pd.read_csv(stagesdf2)
        #df.to_excel(self.writer,sheet_name="SimpleTime")
        #df2.to_excel(self.writer,sheet_name="MergeTime")
        
        #self.writer.save()
        #self.append_df_to_excel(self.file,df, sheet_name="simple-sheet")
        self.append_df_to_excel(self.file,df2,sheet_name="Merge-sheet")
        self.append=True #for multiple files, after first file is executed, for second file append should be true
    def append_df_to_excel(self,filename, df, sheet_name='Sheet1', startrow=None,
                       truncate_sheet=False, 
                       **to_excel_kwargs):
        
        
        if 'engine' in to_excel_kwargs:
            to_excel_kwargs.pop('engine')
        
        filename=os.path.realpath(filename)
        fileExists=os.path.exists(filename)
        print("Filename:"+str(filename))
        
        writer=None
        maxcol=0
        if (fileExists):
            try:
                #print("Filename:"+str(filename))
                wb = openpyxl.load_workbook(filename)
                writer = pd.ExcelWriter(filename,engine="openpyxl")
                writer.book=wb
                #print("Writer.book:"+str(writer.book))
                # get the last row in the existing Excel sheet
                # if it was not specified explicitly
                if startrow is None and sheet_name in writer.book.sheetnames:
                    startrow = writer.book[sheet_name].max_row
                    maxcol=writer.book[sheet_name].max_column
                writer.sheets = {ws.title:ws for ws in writer.book.worksheets}
                
            except Exception as ep:
                print("Error:"+str(ep))
                print("Can't read excel file, creating new")
                
        if startrow is None:
            startrow = 0
        if maxcol is None:
            maxcol = 0

        if (writer==None):
            writer = pd.ExcelWriter(filename,engine="openpyxl")
        # write out the new sheet
#        df.replace(df.filter(regex="Unname"),inplace=True)
        #df.replace(to_replace="Unname",value=" ", regex=True,inplace=True)
        #df.drop(df.filter(regex="Unname"),axis=1,inplace=True)
        print("DF:"+str(df))
        #df.to_excel(writer, sheet_name, startrow=startrow, **to_excel_kwargs,index=False)
        print("startcol:"+str(maxcol))
        df.to_excel(writer,sheet_name,startcol=maxcol,**to_excel_kwargs,index=False)
        #data={"col1":["test-value","value2"]}
        #df=pd.DataFrame(data)
        #print("WriterTest:"+str(writer))
        #df.to_excel(writer,sheet_name,index=False)
        
        

        # save the workbook
        #writer.save()
        writer.close()
                
if __name__ == "__main__":
    rw=RecordWriter()
 
    #rw.writeTime("test.prt",[1,2,3,4,5],[1.2,2.2,3.3,4.4,5.5])
    rw.writeTime("C:\\models_final_test\\test\\test.jt",[1,3,5,6,7,5,6],[1.2,3.2,5.5,6.6,7.7,5.5,6.6],["A","B","C","D","E","C","D"],2,2,2,2)
 